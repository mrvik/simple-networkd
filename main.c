#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/if_dl.h>
#include <net/if_media.h>
#include <net/if_mib.h>
#include <net/if_types.h>
#include <net80211/ieee80211.h>
#include <net80211/ieee80211_ioctl.h>
#include <string.h>

#define NUM_NETWORKS 50

struct wifi_network{
    char ssid[IEEE80211_NWID_LEN+1];
    uint8_t bssid[IEEE80211_ADDR_LEN];
    uint8_t signal_strength;
};

static int sock=-1;
static char *adapterName;

static int init_socket(void);
static int get_wifi_networks(struct wifi_network[], size_t num_networks);
static int query_ioctl(uint16_t request, int *value, void *buffer, size_t *buffer_size);
static void add_result_to(struct wifi_network networks[], int position, struct ieee80211req_scan_result *result, char *ssid);
static void sort_results(struct wifi_network networks[]);
static int sort_network(const void* a, const void* b);
static void print_results(struct wifi_network networks[]);
static int findchar(char* string, char character, int max);

int main(int argc, char **argv){
    if(argc<2){
        fprintf(stderr, "First parameter must be a wlan interface\n");
        return EXIT_FAILURE;
    }
    adapterName=argv[1];
    if(init_socket()==-1) return EXIT_FAILURE;
    struct wifi_network *networks=malloc(sizeof(struct wifi_network)*NUM_NETWORKS);
    if(get_wifi_networks(networks, NUM_NETWORKS)==-1) return -1;
    print_results(networks);
    return EXIT_SUCCESS;
}

static int init_socket(){
    if((sock=socket(AF_LOCAL, SOCK_DGRAM, 0))==-1){
        perror(NULL);
        return -1;
    }
    return 0;
}

static int get_wifi_networks(struct wifi_network nets[], size_t num_networks){
    int ssid_length, value=0;
    uint8_t buf[24*1024];
    size_t buf_size=sizeof(buf);
    const uint8_t *cp, *idp;
    char ssid[IEEE80211_NWID_LEN+1];
    struct ieee80211req_scan_result isr;
    if(query_ioctl(IEEE80211_IOC_SCAN_RESULTS, &value, &buf, &buf_size)==-1){
        return -1;
    }
    cp=buf;
    int pos=0;
    do{
        memcpy(&isr, cp, sizeof(struct ieee80211req_scan_result));
        memset(ssid, 0, IEEE80211_NWID_LEN+1);
        if(isr.isr_meshid_len){
            idp=cp+isr.isr_ie_off+isr.isr_ssid_len;
            ssid_length=isr.isr_meshid_len;
        }else{
            idp=cp+isr.isr_ie_off;
            ssid_length=isr.isr_ssid_len;
        }
        if(ssid_length > IEEE80211_NWID_LEN) ssid_length=IEEE80211_NWID_LEN;
        memcpy(ssid, idp, ssid_length);
        ssid[IEEE80211_NWID_LEN]='\0';
        (void) add_result_to(nets, pos++, &isr, ssid);
        cp+=isr.isr_len;
        buf_size-=isr.isr_len;
    }while(buf_size >= sizeof(struct ieee80211req_scan_result));
    sort_results(nets);
    return 0;
}

static int query_ioctl(uint16_t request, int *value, void *buffer, size_t *buffer_size){
    struct ieee80211req ireq;
    memset(&ireq, 0, sizeof(struct ieee80211req));
    strlcpy(ireq.i_name, adapterName, IFNAMSIZ);
    ireq.i_type=request;
    ireq.i_val=*value;
    ireq.i_len=*buffer_size;
    ireq.i_data=buffer;
    if(ioctl(sock, SIOCG80211, &ireq)<0){
        perror(NULL);
        return -1;
    }
    *buffer_size=ireq.i_len;
    *value=ireq.i_val;
    return 0;
}

static void add_result_to(struct wifi_network array[], int position, struct ieee80211req_scan_result *isr, char *ssid){
    struct wifi_network net;
    strcpy(net.ssid, ssid);
    net.signal_strength=isr->isr_rssi;
    array[position]=net;
}

static void print_results(struct wifi_network networks[]){
    struct wifi_network actual;
    puts("SSID");
    for(int i=0; i<NUM_NETWORKS; i++){
        actual=networks[i];
        if(findchar(actual.ssid, '\0', sizeof(actual.ssid))<=0) continue;
        printf("%2d: SSID: %-20s SIG: %d\n", i, actual.ssid, actual.signal_strength);
    }
}

static int findchar(char* string, char character, int max){
    unsigned int current=0;
    int pos=-1;
    while(current<max && pos==-1){
        if(string[current]==character) pos=current;
        else current++;
    }
    return pos;
}
static void sort_results(struct wifi_network networks[]){
    qsort(networks, NUM_NETWORKS, sizeof(struct wifi_network), sort_network);
}
static int sort_network(const void* a, const void* b){
    struct wifi_network wa=*(const struct wifi_network *) a;
    struct wifi_network wb=*(const struct wifi_network *) b;
    return wb.signal_strength - wa.signal_strength;
}
